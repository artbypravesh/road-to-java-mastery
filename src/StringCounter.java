import java.util.Scanner;
import java.lang.*;

/**
 * Author: Pravesh Narayan
 * Date: 21-Jan-16.
 */
public class StringCounter {
    public static void main(String[] args) {
        Scanner getString = new Scanner(System.in);

        String CountedString;

        System.out.println("What do you want to count?");
        CountedString = getString.nextLine();

        Integer letterCount;
        letterCount = CountedString.length();

        System.out.println("There were " + letterCount + " charcter's in your string.");

    }
}
