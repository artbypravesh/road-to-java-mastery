//Just imports the scanner api
import java.util.Scanner;
//Using * you can import the whole package
import java.lang.*;
/**
 * Created by Pravesh Narayan on 21-Jan-16.
 */

public class SayHello {
    public static void main(String[] args) {
        //Create an instance of the Scanner class
        //use for keyboard access
        Scanner getName = new Scanner (System.in);

        //Create a variable to hold the user name
        //This variable is designed to hold text
        String userName;

        //Ask the user's name and place this
        //in variable userName
        System.out.print("What is your name?");
        userName = getName.nextLine();

        //Display a message with the
        //user's userName
        System.out.println("Hello " + userName + "!");
    }
}
