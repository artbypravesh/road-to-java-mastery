import java.util.Scanner;
import java.lang.*;
/**
 * Author: Pravesh Narayan
 * Date: 21-Jan-16.
 */
public class SayingHello {
    public static void main(String[] args) {
        Scanner getUsername = new Scanner(System.in);

        String Username;

        System.out.println("What is you name?");
        Username = getUsername.nextLine();

        Integer letterCount;
        letterCount = Username.length();

        System.out.println("Hello " + Username + " ! " + "There's " + letterCount + " words in your name.");

    }
}
