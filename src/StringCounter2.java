import java.util.Scanner;

/**
 * Author: Pravesh Narayan
 * Date: 21-Jan-16.
 */
public class StringCounter2 {
    public static void main(String[] args) {
        Scanner getString = new Scanner(System.in);

        String CountedString;

        System.out.println("What is your name?");
        CountedString = getString.nextLine();

        Integer NameLength;
        NameLength = CountedString.length();

        if (NameLength > 5) {
            System.out.println("There's " + NameLength + " character's in you name. You are eligible.");
        }
        else System.out.println("You have " + NameLength + " character's in your name. You are not eligible.");
    }
}
